# Strikeout

An OpenPowerlifting extension for LibreOffice 7.0.

When installed, it adds a button to the "Standard" toolbar that negates
the value of every cell with strikethrough formatting.

This automates a time-consuming and error-prone transformation commonly
required for competition data from ex-USSR countries.

## Installation

1. Run `make` to create the `strikeout.oxt` extension file.
2. In LibreOffice Calc, select `Tools -> Extension Manager`.
3. Press `Add` and select the `strikeout.oxt`. Accept the license.
4. Press `Close`, which will prompt you to restart LibreOffice.
