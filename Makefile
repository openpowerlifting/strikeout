strikeout.oxt:
	zip -r strikeout.oxt Addons.xcu description.xml icons META-INF pkg-desc registration StrikeoutLibrary

.PHONY: clean
clean:
	rm -f strikeout.oxt
